/*! Data structures describing the game state */

use serde::Deserialize;

use rand::seq::SliceRandom;

#[derive(Copy, Clone)]
pub enum Affiliation {
    Red,
    Blue
}

impl std::fmt::Display for Affiliation {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        fmt.write_str(match self {
            Affiliation::Red => "Red",
            Affiliation::Blue => "Blue",
        })
    }
}

impl Affiliation {
    fn opposite(&self) -> Affiliation {
        match self {
            Affiliation::Red => Affiliation::Blue,
            Affiliation::Blue => Affiliation::Red,
        }
    }
}

/// Enumeration of all things one can take to put in front of oneself.
///
/// This includes both Tokens and the small cards.
// Deserialize is only in here to aid the server module
#[derive(Copy, Clone, Debug, PartialEq, Deserialize)]
pub enum Takeable {
    Rank,
    ColorToken,
    Unknown,

    Feather,
    Fan,
    Wand,
    GreenShield,
    GreenSword,
    PurpleShield,
    PurpleSword,
}

impl Takeable {
    /// An easier to-string converter that can profit from the knowledge of all types having static
    /// str names. This is used both in serialization to forms, and for CSS labels.
    pub fn label(&self) -> &'static str {
        match self {
            Takeable::Rank => "Rank",
            Takeable::ColorToken => "ColorToken",
            Takeable::Unknown => "Unknown",
            Takeable::Feather => "Feather",
            Takeable::Fan => "Fan",
            Takeable::Wand => "Wand",
            Takeable::GreenShield => "GreenShield",
            Takeable::GreenSword => "GreenSword",
            Takeable::PurpleShield => "PurpleShield",
            Takeable::PurpleSword => "PurpleSword",
        }
    }
}

#[derive(Copy, Clone)]
pub struct Rank(pub i8);

// When Inquisitor is added, this will become an Enum
#[derive(Copy, Clone)]
pub struct Card {
    pub rank: Rank,
    pub affiliation: Affiliation,
}

impl std::fmt::Display for Card {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(fmt, "{} {}", self.affiliation, self.rank.0)
    }
}

impl std::fmt::Debug for Card {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(fmt, "Card {{ {} {} }}", self.affiliation, self.rank.0)
    }
}

impl Card {
    pub fn get_clue(&self) -> Affiliation {
        match self.rank {
            Rank(3) => self.affiliation.opposite(),
            _ => self.affiliation,
        }
    }
}

/// Identifier for players
///
/// These need to be kept secret from other players.
///
/// It is typically passed out and in using Display and FromString (parse).
#[derive(Debug, Copy, Clone, PartialEq, Deserialize)]
pub struct PlayerId(u32);

impl PlayerId {
    fn new() -> Self {
        PlayerId(rand::random())
    }
}

impl std::fmt::Display for PlayerId {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(fmt, "{}", self.0)
    }
}

impl std::str::FromStr for PlayerId {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(PlayerId(s.parse()?))
    }
}

/** Complete description of a game
 *
 * The sequence of players is denoted by their order in the data structure.
 *
 * No global data needs to be kept, as all is in cards or token lying in front of players, tracked
 * there. The presence or absence of cards or tokens never needs to be evaluated, as the original
 * game makes sure there is enough of each. (There's even enough swords and shields for corner
 * cases of healing back the six).
 *
 * */
#[derive(Clone, Debug)]
pub enum Game {
    Initializing(Vec<(PlayerId, String)>),
    Running(Vec<Player>),
    Reveal(Vec<Player>),
}

impl Game {
    pub fn empty() -> Game {
        Game::Initializing(Vec::new())
    }

    /// A game with players of deterministic PlayerIds, useful to have a read-to-debug server
    pub fn demo() -> Game {
        let mut g = Game::Initializing(
            vec![(1, "One"), (2, "Two"), (3, "Three"), (4, "Four")]
            .iter()
            .map(|(id, label)| (PlayerId(*id), label.to_string()))
            .collect()
            );
        g.start().expect("Startable by construction");
        g
    }

    pub fn add_player(&mut self, name: String) -> Result<PlayerId, ()> {
        match self {
            Game::Initializing(players) => {
                let id = PlayerId::new();
                players.push((id, name));
                Ok(id)
            },
            _ => Err(()),
        }
    }

    pub fn shuffle(&mut self) -> Result<(), ()> {
        match self {
            Game::Initializing(players) => {
                let mut rng = rand::rngs::OsRng;
                players[..].shuffle(&mut rng);
                Ok(())
            },
            _ => Err(()),
        }
    }

    pub fn start(&mut self) -> Result<(), &'static str> {
        let players = match self {
            Game::Initializing(players) => players,
            _ => return Err("Game is not in starting stage"),
        };

        if players.len() % 2 != 0 {
            return Err("Game can not be started with an odd number of players");
        }

        if players.len() > 18 {
            // The limit per the rules is lower, but this is what's required to not lose players in
            // the course
            return Err("Too many players");
        }

        let mut rng = rand::rngs::OsRng;

        let mut cards = Vec::with_capacity(players.len());
        let cards_each_affiliation = players.len() / 2;
        let mut ranks: Vec<_> = (1..9).map(|n| Rank(n)).collect();
        ranks.shuffle(&mut rng);
        cards.extend(ranks[..cards_each_affiliation].iter().map(|&r| Card { rank: r, affiliation: Affiliation::Red }));
        ranks.shuffle(&mut rng);
        cards.extend(ranks[..cards_each_affiliation].iter().map(|&r| Card { rank: r, affiliation: Affiliation::Blue }));

        cards.shuffle(&mut rng);

        *self = Game::Running(
            players
            // I'd prefer to drain this (this would spare me the later replacement that keeps the
            // players memory-valid); but mutably interating and swapping the value out makes
            // things work without allocation.
            .iter_mut()
            .zip(cards)
            .map(|((id, ref mut name), c)| Player::starting(*id, core::mem::replace(name, String::new()), c))
            .collect()
            );

        Ok(())
    }

    pub fn reveal(&mut self) -> Result<(), ()> {
        match self {
            Game::Running(ref mut players) => {
                let players = core::mem::replace(players, Vec::new());
                *self = Game::Reveal(players);
                Ok(())
            }
            _ => Err(())
        }
    }

    pub fn unstart(&mut self) {
        match self {
            Game::Running(ref mut players) | Game::Reveal(ref mut players) => {
                *self = Game::Initializing(
                    players.drain(..)
                        .map(|p| (p.id, p.name))
                        .collect()
                );
            },
            _ => ()
        }
    }

    /// Given a PlayerId, find theindex in the player vector
    pub fn find_player(&self, id: PlayerId) -> Option<usize> {
        match self {
            Game::Initializing(players) => players.iter().position(|x| x.0 == id),
            Game::Running(players) | Game::Reveal(players) => players.iter().position(|x| x.id == id),
        }
    }

    /// If the game is active and the id valid, obtain mutable access to the player
    ///
    /// Error state may become something like "game not active" or "no such player" later.
    pub fn find_active_player_mut(&mut self, id: PlayerId) -> Result<&mut Player, ()> {
        let id = self.find_player(id).ok_or(())?;
        if let Game::Running(ref mut players) = self {
            Ok(&mut players[id])
        } else {
            Err(())
        }
    }

    pub fn kick(&mut self, index: usize) -> Result<(), ()> {
        if let Game::Initializing(players) = self {
            if index >= players.len() {
                return Err(())
            }
            players.remove(index);
            Ok(())
        } else {
            // Can't kick someone without unstarting the game first (it can't be continued without
            // that player)
            Err(())
        }
    }
}

#[derive(Clone, Debug)]
pub struct Player {
    pub id: PlayerId,
    pub name: String,
    pub card: Card,
    /// Quite an unstructured list of rank token, color tokens and cards; if more game logic is
    /// implemented, this might need to change.
    ///
    /// It preserves sequence, although it probably shouldn't. (Maybe
    /// https://stackoverflow.com/questions/61007142/is-there-a-common-way-to-express-unions-over-c-style-enums-in-rust
    /// can help).
    pub taken: heapless::Vec<Takeable, heapless::consts::U8>,
    /// Player index the player has taken a peek at
    peekee: heapless::Vec<usize, heapless::consts::U2>,
}

impl Player {
    fn starting(id: PlayerId, name: String, card: Card) -> Self {
        Player {
            id: id,
            name: name,
            card: card,
            taken: heapless::Vec::new(),
            peekee: heapless::Vec::new(),
        }
    }

    /// Get the player indices the player has committed on peeking at
    pub fn get_peeked(&self) -> Option<[usize; 2]> {
        let mut result = [0, 0];
        result.copy_from_slice(self.peekee.get(0..2)?);
        Some(result)
    }

    /// Can the player pick another card to peek?
    ///
    /// Doesn't rule out peeking self, as the own index is unknown with the given data
    pub fn can_peek(&self, index: usize) -> bool {
        self.peekee.len() < 2 &&
            self.taken.iter().any(|x| matches!(x, Takeable::Fan)) &&
            !self.peekee.iter().any(|i| i == &index)
    }

    pub fn peek(&mut self, peekee: usize) -> Result<(), ()> {
        if self.can_peek(peekee) {
            self.peekee.push(peekee).expect("can_peek should have ensured there is room");
            Ok(())
        } else {
            Err(())
        }
    }

    /// Have the player identified by an ID take a given token or item
    ///
    /// No precise error indication is given as to whether the game is not running or player doesn't
    /// exist or whether their stack is full (which can't happen through legal moves)
    pub fn take(&mut self, take: Takeable) -> Result<(), ()> {
        self.taken.push(take)
            .map_err(|_| ())
    }

    pub fn untake(&mut self, untake: Takeable) -> Result<(), ()> {
        // To be simplified when remove_item is stable
        if let Some(index) = self.taken.iter().position(|x| x == &untake) {
            self.taken[index..].rotate_left(1);
            self.taken.pop();
            Ok(())
        } else {
            Err(())
        }
    }
}
