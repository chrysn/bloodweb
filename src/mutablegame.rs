/*! Wrapping game states to make them usable globally between serving tasks */

use std::sync::RwLock;
use std::sync::Arc;

use crate::game::Game;

/// Globally usable mutable game state
///
/// This is really a few easy-access methods for RwLock<Arc<Game>>, along with a mechanism to
/// asynchronously notify waiting tasks (websocket writers) of changes.
///
/// On RwLock<Arc<Game>> and its easy-access methods
/// ------------------------------------------------
///
/// This wrapping was chosen because it allows cheaply getting readable copies of the
/// current-at-the-time-of-request game state, and changes are done by replacing the game with an
/// updated copy.
///
/// It'd certainly be nice to have an async RwLock that allows us to wait for the small time until
/// a pending write or read (both of which are, as stated above, short) is finished rather than
/// having to 503 out. Then again, that'd be used in a way where it write-locks from reading to
/// writing, whereas this possibly-503ing variety keep its writing hands off the lock until it
/// actually tries to write. (Under load that would push the contention back to the client who'd
/// hopefully exponentially fall off with repeated 503s, while a nonblocking one would accumulate
/// waiting connections).
///
/// On the queue
/// ------------
///
/// Any reader that wants to be informed of changes can push an end of an async_std queue writer
/// into a listener list. Whenever there is a change, those writers are all dropped, creating an
/// "interrupt" at the other ends. (If any listener ever wants to get more changes, this could be
/// changed into changing messages -- but then they'd need to get the latest state again and
/// syncing gets messy; anyway, as long as the consumer is a shortlived websocket...).
///
/// (I'd have preferred to just have an Arc<Mutex> in there and a locked MutexGuard, which every
/// copy can then wait for being lockable, but a MutexGuard requires a lifetime, and I didn'tmanage
/// that).
pub struct MutableGame {
    game: RwLock<Arc<Game>>,
    listeners: async_std::sync::Mutex<Vec<async_std::sync::Sender<()>>>,
}

impl MutableGame {
    pub fn new(game: Game) -> Self {
        MutableGame {
            game: RwLock::new(Arc::new(game)),
            listeners: async_std::sync::Mutex::new(Vec::with_capacity(10)),
        }
    }

    /// Get an indefinitely usable (but possibly soon outdated) read-only copy
    pub fn read(&self) -> Option<Arc<Game>> {
        Some(self.game.read().ok()?.clone())
    }

    pub async fn enqueue(&self, listener: async_std::sync::Sender<()>) {
        let mut accessor = self.listeners.lock().await;
        accessor.push(listener);
    }

    /// Try to replace the state with the input game state.
    ///
    /// This can fail because there is currently another lock on it, either from a perfectly
    /// simultaneous .read() or a perfectly simultaneous .replace() (as both are short).
    ///
    /// To avoid replacing a state while someone else just replaced it with a different one, the
    /// game state the replacement was built from needs to be passed in as if_match. If that
    /// doesn't match the current state, the replacement fails just as well.
    pub fn replace(&self, replacement: Game, if_match: Arc<Game>) -> Result<(), ()> {
        // Allocating the Arc to hold the owned Game before grabbing the lock could be premature
        // optimization, or could just be following best practice. It might also actually be bad as
        // it precludes reusing the memory of the if_match that's just being freed.
        let replacement = Arc::new(replacement);
        let mut place = self.game.write().map_err(|_| ())?;
        let mut queuelock = self.listeners.try_lock().ok_or(())?;
        if &**place as *const _ != &*if_match as *const _ {
            Err(())
        } else {
            *place = replacement;
            // Freeing the senders makes all their receivers return None
            queuelock.clear();
            Ok(())
        }
    }
}
