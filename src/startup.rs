/*! Server start-up and command line handling */

use crate::mutablegame::MutableGame;
use crate::game::Game;

use structopt::StructOpt;

#[derive(StructOpt)]
struct StartupOptions {
    /// Start the server with a game running that has players /1 to /4
    #[structopt(long)]
    demo_mode: bool,
    /// IP address and port to bind to
    #[structopt(long, default_value = "[::]:3000")]
    bind: std::net::SocketAddr,
}

pub async fn main() {
    let opt = StartupOptions::from_args();

    let game = if opt.demo_mode {
        Game::demo()
    } else {
        Game::empty()
    };

    // Conceptually, having one MutableGame on the stack and passing references to it to the
    // threads and tasks would sufficel practically, not even a &'static I could get from a leaked
    // box is good enough for some reaons, so putting it into an Arc and having the threads and
    // tasks refcount (should only happen early / at startup).
    //
    // (I'm currently rationalizing this as "This allows the tasks and threads to live on even
    // beyond the runtime of main, and the game state persists as long as any of them is there to
    // keep it alive").
    let mastergame = std::sync::Arc::new(MutableGame::new(game));

    warp::serve(crate::server::build_routes(mastergame)).run(opt.bind).await;
}
