#![recursion_limit="2048"]

mod startup;
mod server;
mod game;
mod mutablegame;

#[tokio::main]
async fn main() {
    startup::main().await
}
