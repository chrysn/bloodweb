/*! Implementations of the actual resources */

use http::StatusCode;

use typed_html::{html, text};
use serde::Deserialize;

use crate::game::{Game, PlayerId, Takeable, Affiliation, Rank, Card};
use crate::mutablegame::MutableGame;

#[derive(Debug)]
pub struct ServeError {
    code: StatusCode,
    message: &'static str,
}

impl ServeError {
    pub async fn recover(err: warp::reject::Rejection) -> Result<impl warp::reply::Reply, warp::reject::Rejection> {
        if let Some(ServeError { code, message }) = err.find::<ServeError>() {
            Ok(warp::reply::with_status(message.to_string(), *code))
        } else {
            Err(err)
        }
    }
}

trait ErrWithExt {
    type OkType;

    fn err_with(self, code: StatusCode, msg: &'static str) -> Result<Self::OkType, ServeError>;
}

impl<T, E> ErrWithExt for Result<T, E> {
    type OkType = T;

    fn err_with(self, code: StatusCode, message: &'static str) -> Result<Self::OkType, ServeError> {
        self.map_err(|_| ServeError { code, message })
    }
}

impl<T> ErrWithExt for Option<T> {
    type OkType = T;

    fn err_with(self, code: StatusCode, message: &'static str) -> Result<Self::OkType, ServeError> {
        self.ok_or_else(|| ServeError { code, message })
    }
}

impl warp::reject::Reject for ServeError {}

impl From<ServeError> for warp::reject::Rejection {
    fn from(e: ServeError) -> Self {
        warp::reject::custom(e)
    }
}

// Common forms
#[derive(Deserialize)]
struct Taken { take: Takeable }
#[derive(Deserialize)]
struct Playerdata { name: String }
#[derive(Deserialize)]
struct Kickdata { whom: usize }
#[derive(Deserialize)]
struct Startoptions { shuffle: Option<String> } // FIXME: a boolean would be nice, but is not compatible with checkboxes' behavior
#[derive(Deserialize)]
struct Peek { index: usize }

pub fn build_routes(mastergame: std::sync::Arc<MutableGame>) -> warp::filters::BoxedFilter<(impl warp::Reply,)> {
    use warp::Filter;

    // Only async because it's fallible, and warp's and_then needs async
    async fn find_player(
        snapshot: std::sync::Arc<Game>,
        p: Option<PlayerId>
    ) -> Result<(std::sync::Arc<Game>, Option<PlayerId>, Option<usize>), warp::reject::Rejection>
    {
        let playerindex = p
                .map(|id| snapshot.find_player(id)
                    .err_with(StatusCode::NOT_FOUND, "No such player in current game"))
                .transpose()?;
        Ok((snapshot, p, playerindex))
    };

    async fn process_form(
        snapshot: std::sync::Arc<Game>,
        mut playerid: Option<PlayerId>,
        _: Option<usize>,
        query: String,
        payload: bytes::Bytes,
        mgc: std::sync::Arc<MutableGame>
    )-> Result<impl warp::reply::Reply, warp::reject::Rejection> {
        let mut newgame: Game = (*snapshot).clone();

        match (query.as_ref(), playerid) {
            ("join", None) => {
                let playerdata: Playerdata = serde_urlencoded::from_bytes(&payload)
                    .err_with(StatusCode::BAD_REQUEST, "Wrong form")?;

                playerid = Some(newgame.add_player(playerdata.name)
                    .err_with(StatusCode::SERVICE_UNAVAILABLE, "Please retry")?);
            }
            // Not ruling out that an observer starts the game
            ("start", _) => {
                let startoptions: Startoptions = serde_urlencoded::from_bytes(&payload)
                    .err_with(StatusCode::BAD_REQUEST, "Wrong form")?;

                if startoptions.shuffle.is_some() {
                    newgame.shuffle()
                        .err_with(StatusCode::INTERNAL_SERVER_ERROR, "Failed to start the game")?;
                }

                newgame.start()
                    // FIXME make str error forwardable
                    // FIXME code? "precondition failed"?
                    .err_with(StatusCode::INTERNAL_SERVER_ERROR, "Failed to start the game")?;
            }

            // FIXME simplify once or_patterns is a stable feature
            (query, Some(id)) if query == "take" || query == "untake" || query == "peek" => {
                let player = newgame.find_active_player_mut(id)
                        .err_with(StatusCode::NOT_FOUND, "No such player")?;

                match query {
                    "take" => {
                        let taken: Taken = serde_urlencoded::from_bytes(&payload)
                            .err_with(StatusCode::BAD_REQUEST, "Wrong form")?;

                        player.take(taken.take)
                            .err_with(StatusCode::PRECONDITION_FAILED, "Can't take any more cards")?;
                    }
                    "untake" => {
                        let taken: Taken = serde_urlencoded::from_bytes(&payload)
                            .err_with(StatusCode::BAD_REQUEST, "Wrong form")?;

                        player.untake(taken.take)
                            .err_with(StatusCode::PRECONDITION_FAILED, "Can't untake that")?;
                    }
                    "peek" => {
                        let data: Peek = serde_urlencoded::from_bytes(&payload)
                            .err_with(StatusCode::BAD_REQUEST, "Wrong form")?;

                        player.peek(data.index)
                            .err_with(StatusCode::BAD_REQUEST, "Can't peek right now")?;
                    }
                    _ => unreachable!()
                }
            }

            ("reveal", _) => {
                newgame.reveal()
                    .err_with(StatusCode::PRECONDITION_FAILED, "Game not running")?;
            }

            ("delete", _) => {
                newgame.unstart();
            }

            ("kick", _) => {
                let kickdata: Kickdata = serde_urlencoded::from_bytes(&payload)
                    .err_with(StatusCode::BAD_REQUEST, "Wrong form")?;

                newgame.kick(kickdata.whom)
                    .err_with(StatusCode::PRECONDITION_FAILED, "Can't kick")?;
            }

            _ => return Err(warp::reject::not_found()),
        }

        mgc.replace(newgame, snapshot)
            .err_with(StatusCode::SERVICE_UNAVAILABLE, "Please retry")?;

        Ok(warp::reply::with_header(
            StatusCode::SEE_OTHER,
            http::header::LOCATION,
            playerid.map(|p| format!("{}", p)).unwrap_or(".".to_string())
            ))
    };

    let core = warp::any()
        .map({ let mgc = mastergame.clone(); move || mgc.read().unwrap() })
        .and(
            warp::path::end().map(|| None)
            .or(warp::path!(PlayerId).map(|p| Some(p)))
            .unify()
        )
        .and_then(find_player)
        .untuple_one()
        ;
    // Do I really need to clone here, or couldn't I chain those?
    let getting = core.clone()
        .and(warp::get())
        // "and don't take any query parameters"
        .and(warp::filters::query::query::<()>().untuple_one())
        .map(|snapshot: std::sync::Arc<_>, _playerid, playerindex| warp::reply::html(show_gamestate(&snapshot, playerindex).to_string()))
        ;
    let posting = core
        .and(warp::post()
             .and(warp::body::content_length_limit(1024))
             .and(warp::filters::query::raw())
             .and(warp::filters::body::bytes()) // using .form() would be great, but it seems I can't route based on static query strings to select the right path before .and()ing
        )
        // FIXME evaluate whether that's necessary
        .and(warp::any().map({ let mgc = mastergame.clone(); move || mgc.clone() }))
        .and_then(process_form)
        ;

    let staticdir = warp::path("static").and(warp::fs::dir("./static/"));

    use futures::SinkExt;
    async fn on_upgrade(
        mut websocket: warp::filters::ws::WebSocket,
        mgc: std::sync::Arc<MutableGame>,
    ) {
        let (s, r) = async_std::sync::channel(1);
        mgc.enqueue(s).await;
        r.recv().await;
        // Really anything will trigger the client to reload
        websocket.send(warp::filters::ws::Message::text("")).await
            .unwrap_or_else(|e| eprintln!("Failed to send notification: {:?}", e));
        websocket.close().await
            .unwrap_or_else(|e| eprintln!("Failed to close socket: {:?}", e));
    }
    let ws = warp::path::end()
        .and(warp::ws())
        .map({
            let mgc = mastergame.clone();
            move |ws: warp::ws::Ws| {
                let mgc = mgc.clone();
                // And then our closure will be called when it completes...
                ws.on_upgrade(move |websocket| { on_upgrade(websocket, mgc.clone()) })
            }
        });

    warp::any()
        .and(ws)
        .or(getting)
        .or(posting)
        .or(staticdir)
        .recover(ServeError::recover)
        .boxed()
}

/// A vector of Option<Takeable>s that has a fixed sequence of (rank or None, most colorful token
/// or None, other token or None, ...any cards in their sequence).
///
/// Constructing one erases all sequence except for the non-token items.
///
/// Some invalid game information (multiple ranks, >2 affiliations) is silently made invisible in
/// the process.
struct SlottedTakeables {
    items: heapless::Vec<Option<Takeable>, heapless::consts::U8>,
}

impl SlottedTakeables {
    fn new<'a>(taken: impl Iterator<Item=&'a Takeable>) -> Self {
        let mut first_affiliation_set = false;

        let mut items = heapless::Vec::new();
        items.push(None).expect("Succeeds per size construction"); // Rank
        items.push(None).expect("Succeeds per size construction"); // First affiliation token
        items.push(None).expect("Succeeds per size construction"); // Second affiliation token

        for t in taken {
            let current = Some(*t);
            match *t {
                Takeable::Rank => {
                    items[0] = current;
                },
                Takeable::Unknown | Takeable::ColorToken => {
                    if first_affiliation_set {
                        if let Some(Takeable::Unknown) = items[1] {
                            // Ensure it's color first, unknown second
                            items[2] = items[1].take();
                            items[1] = current;
                        } else {
                            items[2] = current;
                        }
                    } else {
                        items[1] = current;
                        first_affiliation_set = true;
                    }
                },
                // If someone has filled their taken up to the brink with items, this can overflow
                // items, but that's already an invalid game state as the player's takeables are
                // large enough to hold any valid item combination plus the 3 tokens whose spaces
                // make things tighter here -- so it's already an invalid game state.
                _ => items.push(current).unwrap_or(()),
            }
        }

        Self { items }
    }

    // Given it's only used for show_gamstate, this is easier than implementing IntoIter or similar
    fn iter(&self) -> impl Iterator<Item=&Option<Takeable>> {
        self.items.iter()
    }
}

fn show_gamestate(game: &Game, playerindex: Option<usize>) -> impl std::fmt::Display {
    let tree = match game {
        Game::Initializing(players) => html!(<div>
            <h1> "Preparing BloodWeb game" </h1>
            <p> "Current participants:" </p>
            <ul>
            {
                players.iter().enumerate().map(|(index, (_id, name))| html! {
                    <li> { text!(name) } <form action="?kick" method="post"><input type="hidden" name="whom" value={ format!("{}", index) } /><button> "Kick" </button></form> </li>
                })
            }
            </ul>
            { match playerindex {
                Some(x) => html! {
                    <form action="?start" method="post">
                        <label><input type="checkbox" name="shuffle" /> "Shuffle" </label>
                        <button> "Start game" </button>
                    </form>
                },
                None => html! {
                    <form action="?join" method="post">
                        <label>"Name: " <input type="text" name="name" required=true /></label>
                        <button> "Join" </button>
                    </form>
                },
            }}

        </div>),

        Game::Running(players) | Game::Reveal(players) => {
            let me = playerindex.map(|i| &players[i]);
            let peeked = me.and_then(|m| m.get_peeked());


            html!(<div>
            <h1> "BloodWeb running" </h1>
            <p> "Game state:" </p>
            <ul>
            {
                players.iter().enumerate().map(|(i, p)| {
                    let slotted = SlottedTakeables::new(p.taken.iter());
                    let mut slotted = slotted
                        .iter()
                        .map(|t| RenderableTakeable(t.as_ref(), &p.card).render());

                    html! {
                    <li>
                        <span>
                            { slotted.next().expect("available per slotting") } " "
                            { slotted.next().expect("available per slotting") } " "
                            { slotted.next().expect("available per slotting") } " "
                        </span>
                        { text!("{}", p.name) }
                        { if Some(i) == playerindex {
                            text!(" (you: {})", p.card)
                        } else if matches!(game, Game::Reveal(_)) {
                            text!(" (was: {})", p.card)
                        } else if peeked.map(|p| p.iter().any(|p| p == &i)) == Some(true) {
                            text!(" (peeked: {})", p.card)
                        } else if me.map(|p| p.can_peek(i)) == Some(true) {
                            html!(<form style="display:inline" action="?peek" method="post"><input type="hidden" name="index" value=(format!("{}", i)) /><button class="peek"> "Peek" </button></form>)
                        } else {
                            // FIXME I bet there's a more elegant way to express this, which is
                            // actually all the type hint the other branches need
                            let empty = text!("");
                            let empty: Box<dyn typed_html::elements::FlowContent<_>> = Box::new(*empty);
                            empty
                        } }
                        { if Some((i + 1) % players.len()) == playerindex { text!(" (clue: {})", p.card.get_clue()) } else { text!("") } }
                        <span>{ WhiteSpaceSeparated::new(slotted) }</span>
                    </li>
                    }
                })
            }
            </ul>
            { if matches!(game, Game::Reveal(_)) {
                html! { <div> "Game ended" </div> }
            } else if let Some(playerindex) = playerindex {
                let button_of = |t: &Takeable| html! { <button name="take" value=(t.label())> { RenderableTakeable(Some(t), &players[playerindex].card).render() } </button> };
                html! {
                <div><form action="?take" method="post">
                    "Take tokens:"
                    { [Takeable::Rank, Takeable::ColorToken, Takeable::Unknown]
                            .iter() .map(button_of) }
                    "Take items:"
                    { [Takeable::Feather, Takeable::Fan, Takeable::Wand, Takeable::GreenShield, Takeable::GreenSword, Takeable::PurpleShield, Takeable::PurpleSword]
                            .iter() .map(button_of) }
                </form>
                <form action="?untake" method="post">
                    "Heal tokens:"
                    { [Takeable::Rank, Takeable::ColorToken, Takeable::Unknown]
                            .iter() .map(button_of) }
                    "Undo item:"
                    { [Takeable::Feather, Takeable::Fan, Takeable::Wand, Takeable::GreenShield, Takeable::GreenSword, Takeable::PurpleShield, Takeable::PurpleSword]
                            .iter() .map(button_of) }
                </form></div> }
            } else {
                html! { <div> "Spectator mode" </div> }
            } }
            <hr />
            {
                // It seems details can't be constructed via html!, because it needs the special
                // summary element passed in at construction
                let mut d = typed_html::elements::details::new({
                    let mut s = typed_html::elements::summary::new();
                    s.children.append(&mut vec![text!("End game")]);
                    Box::new(s)});
                if matches!(game, Game::Running(_)) {
                    // At least, conditional children are much easier that way
                    d.children.push(html! {
                        <form action="?reveal" method="post">
                            <button> "End game and reveal" </button>
                        </form>
                    });
                }
                d.children.push(html! {
                    <form action="?delete" method="post">
                        <button> "Delete game" </button>
                    </form>
                });
                Box::new(d)
            }
        </div>)},
    };

    // meta: State to modern-day browsers that we use HTML as it was always intended
    // rel:license: I'd like to have something that says "program that's being run on the server
    // side", but a) there's nothing in the registry right now and b) typed-html is picky here.
    // Maybe impl-info?
    let html: typed_html::dom::DOMTree<String> = html! {
        <html>
            <head>
                <title> "BloodWeb" </title>
                <meta name="viewport" content="width=device-width" />
                <link rel="stylesheet" href="static/style.css" />
                <script src="static/ws-based-autorefresh.js" />
                <link rel="license" href="https://gitlab.com/chrysn/bloodweb/" />
            </head>
            <body>
                { tree }
            </body>
        </html>
    };

    html
}

trait Render {
    fn render(&self) -> Box<dyn typed_html::elements::PhrasingContent<String>>;
}

impl Render for Affiliation {
    fn render(&self) -> Box<dyn typed_html::elements::PhrasingContent<String>> {
        let lowerchar = match self { Affiliation::Red => "r", Affiliation::Blue => "b" };
        let upperchar = match self { Affiliation::Red => "R", Affiliation::Blue => "B" };
        html! { <span class=["affiliation", lowerchar]> { text!(upperchar) } </span> }
    }
}

impl Render for Rank {
    fn render(&self) -> Box<dyn typed_html::elements::PhrasingContent<String>> {
        html! { <span class=["rank"]> { text!("{}", self.0) } </span> }
    }
}

struct RenderableTakeable<'a>(Option<&'a Takeable>, &'a Card);

impl<'a> Render for RenderableTakeable<'a> {
    fn render(&self) -> Box<dyn typed_html::elements::PhrasingContent<String>> {
        match self.0 {
            Some(Takeable::Rank) => self.1.rank.render(),
            Some(Takeable::ColorToken) => self.1.affiliation.render(),
            // In alignment with Affiliation rendering
            Some(Takeable::Unknown) => html! { <span class=["affiliation", "unknown"]> "?" </span> },
            Some(x) => html! { <span class=["item", x.label()]> { text!("{:?}", x) } </span> },
            // In alignment, and especially optical alignment, with affiliation and rank
            None => html! { <span class=["blanktoken"]> "\u{a0}" </span> } // no-break space
        }
    }
}

impl Render for str {
    fn render(&self) -> Box<dyn typed_html::elements::PhrasingContent<String>> {
        text!("{}", self)
    }
}

/// Iterator wrapper that creates text!(" ") nodes between and around the iterated-over items
struct WhiteSpaceSeparated<I>(I, bool);

impl<I> WhiteSpaceSeparated<I> {
    pub fn new(i: I) -> Self {
        WhiteSpaceSeparated(i, true)
    }
}

// Actually "with whitespace after", as we don't want to peek
impl<I: Iterator<Item=Box<dyn typed_html::elements::PhrasingContent<String>>>> Iterator for WhiteSpaceSeparated<I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        if self.1 {
            self.1 = false;
            Some(text!(" "))
        } else {
            self.1 = true;
            self.0.next()
        }
    }
}
