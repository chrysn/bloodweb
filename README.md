BloodWeb – an online Blood Bound clone
======================================

BloodWeb is an online playable clone
of Kalle Krenzer's [Blood Bound](https://www.fantasyflightgames.com/en/products/blood-bound/) game.

The game is run as a web server,
and players can join using any browser.

The game is expected to be played with text or voice chat in parallel,
which is not provided by the implementation,
and is typically used by the initiator of the game to invite the players.

The server will largely not try to enforce game rules,
and is not even aware of whose turns it is.
It merely serves to replicate a consistent view of what players place on the table in front of them,
and to facilitate the randomization steps and the selective revealing.

While the abstract rules are not exactly protected by copyright,
BloodWeb will not attempt to replicate them,
but expects them to be known.
(It does provide a web version of the cheat,
as in an online game maybe not *ever* player has one of their own).

The odd-number Inquisitor player mode is not implemented yet.

Usage
-----

To start a server, ensure that any recent version of [Rust](https://www.rust-lang.org/) is installed, and run:

```shell
$ cargo run
```

It opens a public (bound to all addresses) HTTP server on port 3000.
It runs a single game session that can be joined by anyone.

Back-end choices
----------------

While early versions of this were implemented based on bare [hyper](https://hyper.rs/) (which went very well),
the need for real-time updates through websockets and the lack of an easily accessible websocket implementation based on hyper
triggered a move to [warp](https://docs.rs/warp/0.2.2/warp/).
The application works fine on that,
but the complexity of the routing has suffered somewhat
(compare the linear code extracting the player index [in the old version](https://gitlab.com/chrysn/bloodweb/-/blob/9341741d9b736635048fdc3ac0ee6a99b2fb0eb4/src/server.rs#L59) with the description-constructing [new version](https://gitlab.com/chrysn/bloodweb/-/blob/master/src/server.rs#L67).
Some of this can easily be due to lack of understanding of Warp's capabilities, though.

[Tokio](https://tokio.rs/) is included as a direct dependency only to get warp started in a straightforard fashion.
[async-std](https://async.rs/) is used because it provides easy to use asynchronous mutexes through which notification can be implemented.

Where data size is bounded, small and does not need to be passed by ownership (which is the case practically throughout the game),
data structures are limited in size using the [heapless](https://docs.rs/heapless/0.5.4/heapless/) crate.
This is primarily a habit from embedded development,
and has the nice side effect of enforcing a clear error path before data of arbitrary size can be entered by players.

Future development
------------------

A version of this game that can fully run in the user's browser
(ie. can be served from a static server),
uses peer-to-peer communication between the players
and has no single party that could cheat
(by running the server and inspecting its state)
would be a nice replacement for this.

(See [some collected notes on online tabletop gaming](http://christian.amsuess.com/idea-incubator/online-tabletop/) for further thoughts and references for such a version;
in particular, [experiments with a decentralized shuffling algorithm](https://gitlab.com/chrysn/demo-multiparty-secret-permutations) have been started).

Some thought is given during development of this version
about possible future code sharing with an HTML based version
(eg. by using [`typed_html`, which can be used with dodrio](https://github.com/bodil/typed-html/tree/359a7a66ed65a48c9e5a1714fcdc8bbf8ac1fb16/examples/dodrio)).
An ultimately abstracted version,
where the same Rust code could either be built into a WASM/HTML application
for decentralized operation
or into a version executed on a (possibly owned, possibly multi-tenant) server
that participates in the same protocol
would be neat,
but the author knows no tooling that would allow that.
(Among other things,
abstract clickable items would need to become either single-button POST forms to the server,
or (not necessarily form-based) buttons that trigger WASM).
